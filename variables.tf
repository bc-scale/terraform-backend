variable "namespace" {
  type        = "string"
  default     = "sk"
  description = "Organization namespace"
}

variable "stage" {
  type        = "string"
  description = "Stage, e.g. 'prd', 'stg', 'dev' or 'tst'"
}

variable "name" {
  type        = "string"
  default     = "terraform"
  description = "Solution name"
}

variable "tags" {
  type        = "map"
  description = "Additional tags"

  default = {
    "Owner"  = "SkaleSys"
    "Office" = "Perth"
  }
}

variable "region" {
  type        = "string"
  default     = "ap-southeast-2"
  description = "AWS Region"
}
